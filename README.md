# Laravel South African ID number validator.

## Installation
* Run the following n your project root:
```
composer require lochinvarwest/sa-id-validation
```

## Instantiation and Usage

To instantiate, use either:
```php
$validator = SAIdValidator::create($id_number);
```
or:
```php
$validator = new SAIdValidator($id_number);
```
or use the Request Rule validator
```php
'idno' => ['required', new SAIdNumber()]
```

## Available Methods:

### Validate
The validate() method checks the number of digits, that the ID number represents a valid birth date and that the Luhn algorithm checksum digit is correct.
It returns 'true' on success or an error message on failure.
```php
$validator->validate(); // or as above
```

### Check String Length
The validateLength() method checks that the ID number has 13 digits
```php
$validator->validateLength();
```

### Check DoB digits
The validateDateOfBirth() method checks that the ID number represents a valid date for the date of birth.
```php
$validator->validateDateOfBirth();
```
### Get DoB
The getDateOfBirth() method returns a Carbon date object of the date of birth denoted in the provided ID number.
```php
$validator->getDateOfBirth();
```
### Check Checksum digit
The validateChecksum() method checks that the Luhn algorith checksum digit is correct for the ID number supplied.
```php
$validator->validateChecksum();
```
### Check Gender
The validateGender($gender) method checks that the gender supplied corresponds to the gender denoted in the ID number.  The argument is "Female: or "Male" or "F" or "M" (case insensitive)
```php
$validator->validateGender($gender);
```
### Get Gender
The getGender() method returns the gender denoted in the ID number.
```php
$validator->getGender();
```
### Check Citizenship
The validateCitizen($citizenship) method checks that citizenship or residency denoted in the ID number corresponds to the status provided.  The argument is "citizen: or "resident" (case insensitive)
```php
$validator->validateCitizen();
```
### Get Citizenship
The getCitizen() method returns the citizenship status denoted in the ID number.
```php
$validator->getCitizen();
```
