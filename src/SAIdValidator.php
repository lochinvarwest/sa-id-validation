<?php

namespace LochinvarWest\SaIdValidator;

use Carbon\Carbon;
use LochinvarWest\SaIdValidator\Exceptions\IdNumberChecksumException;
use LochinvarWest\SaIdValidator\Exceptions\IdNumberDateOfBirthException;
use LochinvarWest\SaIdValidator\Exceptions\IdNumberLengthException;

class SAIdValidator
{
    protected $idno;
    const MSG_LENGTH_INVALID = 'The ID number is not the correct length';
    const MSG_DATE_OF_BIRTH_INVALID = 'The ID number date of birth is invalid';
    const MSG_CHECKSUM_INVALID = 'The ID number checksum is invalid';

    protected $message = null;

    public function __construct($idno)
    {
        $this->idno = $idno;
    }

    public static function create($idno) {
        return new self($idno);
    }

    public function validate()
    {
        $this->validateLength()
            ->validateDateOfBirth()
            ->validateChecksum();

        return true;
    }

    public function message()
    {
        return $this->message;
    }

    public function validateLength()
    {
        if (!is_numeric($this->idno) || strlen($this->idno) !== 13) {
            throw new IdNumberLengthException(self::MSG_LENGTH_INVALID);
        }
        return $this;
    }

    public function validateDateOfBirth()
    {
        if (substr($this->idno, 2,2) > 12 || substr($this->idno, 2,2) < 1) {
            throw new IdNumberDateOfBirthException(self::MSG_DATE_OF_BIRTH_INVALID);
        } else {
            if (substr($this->idno, 4, 2) > $this->getDateOfBirth()->daysInMonth || substr($this->idno, 4, 2) < 1) {
                throw new IdNumberDateOfBirthException(self::MSG_DATE_OF_BIRTH_INVALID);
            }
        }

        return $this;
    }

    public function getDateOfBirth() {
        $year = substr($this->idno, 0, 2);
        $year = $year > Carbon::now()->year % 100 ? '19' . $year : '20' . $year;

        return Carbon::createFromDate($year, substr($this->idno, 2,2), substr($this->idno, 4,2));
    }

    public function validateChecksum()
    {
        $total = 0;
        $count = 0;
        for ($i = 0; $i < strlen($this->idno); ++$i)
        {
            $multiplier = $count % 2 + 1;
            $count ++;
            $temp = $multiplier * (int)substr($this->idno, $i, 1);
            $temp = floor($temp / 10) + ($temp % 10);
            $total += $temp;
        }
        $total = ($total * 9) % 10;

        if ($total % 10 != 0) {
            throw new IdNumberChecksumException(self::MSG_CHECKSUM_INVALID);
        }

        return $this;
    }

    public function validateGender($gender)
    {
        if (strtolower(substr($gender, 0,1)) == $this->getGender()) {
            return true;
        }
        return false;
    }

    public function getGender()
    {
        if (substr($this->idno, 6, 1) < 5) {
            return 'f';
        } elseif (substr($this->idno, 6, 1) > 4) {
            return 'm';
        } else {
            return 'Invalid Id number';
        }
    }

    public function validateCitizen($citizenship) // 'citizen' or 'resident'
    {
        if (strtolower($citizenship) == $this->getCitizen()) {
            return true;
        }
        return false;
    }

    public function getCitizen()
    {
        if (substr($this->idno, 10, 1) == 0) {
            return 'citizen';
        } elseif (substr($this->idno, 10, 1) == 1) {
            return 'resident';
        } else {
            return 'Invalid Id number';
        }
    }

}
