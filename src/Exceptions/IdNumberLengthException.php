<?php

namespace LochinvarWest\SaIdValidator\Exceptions;

use Exception;

class IdNumberLengthException extends Exception
{
}
