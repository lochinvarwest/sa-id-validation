<?php

namespace LochinvarWest\SaIdValidator\Exceptions;

use Exception;

class IdNumberChecksumException extends Exception
{
}
