<?php

namespace LochinvarWest\SaIdValidator;

use LochinvarWest\SaIdValidator\Exceptions\IdNumberChecksumException;
use LochinvarWest\SaIdValidator\Exceptions\IdNumberDateOfBirthException;
use LochinvarWest\SaIdValidator\Exceptions\IdNumberLengthException;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Carbon;

class SAIdNumber implements Rule
{
    const MSG_LENGTH_INVALID = 'The ID number is not the correct length';
    const MSG_DATE_OF_BIRTH_INVALID = 'The ID number date of birth is invalid';
    const MSG_CHECKSUM_INVALID = 'The ID number checksum is invalid';

    protected $message = null;

    public function passes($attribute, $value)
    {
        try {
            $this->validateLength($value)
                ->validateDateOfBirth($value)
                ->validateChecksum($value);
        } catch (IdNumberLengthException $e) {
            $this->message = $e->getMessage();

            return false;
        } catch (IdNumberDateOfBirthException $e) {
            $this->message = $e->getMessage();

            return false;
        } catch (IdNumberChecksumException $e) {
            $this->message = $e->getMessage();

            return false;
        }

        return true;
    }

    public function message()
    {
        return $this->message;
    }

    public function isValid($value)
    {
        return $this->passes('id_number', $value);
    }

    protected function validateLength($value)
    {
        if (!is_numeric($value) || strlen($value) !== 13) {
            throw new IdNumberLengthException(self::MSG_LENGTH_INVALID);
        }

        return $this;
    }

    protected function validateDateOfBirth($value)
    {
        if (substr($value, 2,2) > 12 || substr($value, 2,2) < 1) {
            throw new IdNumberDateOfBirthException(self::MSG_DATE_OF_BIRTH_INVALID);
        } elseif (substr($value, 4, 2) > $this->getDateOfBirth($value)->daysInMonth || substr($value, 4, 2) < 1) {
            throw new IdNumberDateOfBirthException(self::MSG_DATE_OF_BIRTH_INVALID);
        }

        return $this;
    }

    protected function validateChecksum($value)
    {
        $total = 0;
        $count = 0;

        for ($i = 0, $iMax = strlen($value); $i < $iMax; ++$i) {
            $multiplier = $count % 2 + 1;
            $count ++;
            $temp = $multiplier * (int)substr($value, $i, 1);
            $temp = floor($temp / 10) + ($temp % 10);
            $total += $temp;
        }

        $total = ($total * 9) % 10;

        if ($total % 10 != 0) {
            throw new IdNumberChecksumException(self::MSG_CHECKSUM_INVALID);
        }


        return $this;
    }

    protected function getDateOfBirth($value)
    {
        $year = substr($value, 0, 2);
        $year = $year > Carbon::now()->year % 100 ? '19' . $year : '20' . $year;

        return Carbon::createFromDate($year, substr($value, 2,2), substr($value, 4,2));
    }
}
